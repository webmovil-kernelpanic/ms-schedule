CREATE TABLE schedules (
    schedule_id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    income_id INTEGER REFERENCES income(income_id),
    outcome_Id INTEGER REFERENCES outcome(outcome_id) NULL,
    income_made_by varchar(16) NOT NULL,
    outcome_made_by varchar(16) NULL
);

CREATE TABLE income (
    income_id SERIAL PRIMARY KEY,
    date TIMESTAMP NOT NULL,
    latitude FLOAT(8) NOT NULL,
    longitude FLOAT(8) NOT NULL,
    altitude FLOAT(8) NOT NULL
);

CREATE TABLE outcome (
    outcome_id SERIAL PRIMARY KEY,
    date TIMESTAMP NOT NULL,
    latitude FLOAT(8) NOT NULL,
    longitude FLOAT(8) NOT NULL,
    altitude FLOAT(8) NOT NULL
);
CREATE TABLE weekly_hours (
  id SERIAL PRIMARY KEY,
  user_id INT NOT NULL,
  week_starts TIMESTAMP WITH TIME ZONE NOT NULL,
  mon_hours FLOAT NULL,
  tue_hours FLOAT NULL,
  wed_hours FLOAT NULL,
  thu_hours FLOAT NULL,
  fri_hours FLOAT NULL,
  sat_hours FLOAT NULL,
  sun_hours FLOAT NULL
);
CREATE TABLE annual_hours (
  id SERIAL PRIMARY KEY,
  user_id INT NOT NULL,
  jan_hours FLOAT NULL,
  feb_hours FLOAT NULL,
  mar_hours FLOAT NULL,
  apr_hours FLOAT NULL,
  may_hours FLOAT NULL,
  jun_hours FLOAT NULL,
  jul_hours FLOAT NULL,
  aug_hours FLOAT NULL,
  sep_hours FLOAT NULL,
  oct_hours FLOAT NULL,
  nov_hours FLOAT NULL,
  dec_hours FLOAT NULL
);
