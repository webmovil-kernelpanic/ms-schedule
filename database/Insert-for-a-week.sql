-- Insertar ingresos y egresos para Enero
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-01-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-01-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-01-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-01-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-01-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-01-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-01-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-01-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-01-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-01-05 20:35:18.906461+00', 40.7128, -74.0060, 10);


-- Insertar ingresos y egresos para Febrero
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-02-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-02-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-02-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-02-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-02-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-02-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-02-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-02-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-02-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-02-05 20:35:18.906461+00', 40.7128, -74.0060, 10);

-- Insertar ingresos y egresos para Marzo
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-03-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-03-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-03-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-03-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-03-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-03-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-03-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-03-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-03-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-03-05 20:35:18.906461+00', 40.7128, -74.0060, 10);

-- Insertar ingresos y egresos para Abril
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-04-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-04-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-04-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-04-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-04-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-04-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-04-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-04-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-04-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-04-05 20:35:18.906461+00', 40.7128, -74.0060, 10);

-- Insertar ingresos y egresos para Mayo
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-05-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-05-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-05-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-05-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-05-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-05-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-05-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-05-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-05-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-05-05 20:35:18.906461+00', 40.7128, -74.0060, 10);


-- Insertar ingresos y egresos para Junio
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-06-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-06-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-06-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-06-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-06-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-06-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-06-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-06-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-06-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-06-05 20:35:18.906461+00', 40.7128, -74.0060, 10);



-- Insertar ingresos y egresos para Julio
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-07-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-07-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-07-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-07-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-07-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-07-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-07-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-07-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-07-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-07-05 20:35:18.906461+00', 40.7128, -74.0060, 10);



-- Insertar ingresos y egresos para Agosto
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-08-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-08-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-08-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-08-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-08-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-08-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-08-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-08-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-08-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-08-05 20:35:18.906461+00', 40.7128, -74.0060, 10);


-- Insertar ingresos y egresos para Septiembre
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-09-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-09-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-09-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-09-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-09-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-09-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-09-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-09-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-09-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-09-05 20:35:18.906461+00', 40.7128, -74.0060, 10);


-- Insertar ingresos y egresos para Octubre
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-10-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-10-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-10-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-10-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-10-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-10-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-10-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-10-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-10-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-10-05 20:35:18.906461+00', 40.7128, -74.0060, 10);



-- Insertar ingresos y egresos para Noviembre
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-11-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-11-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-11-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-11-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-11-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-11-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-11-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-11-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-11-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-11-05 20:35:18.906461+00', 40.7128, -74.0060, 10);


-- Insertar ingresos y egresos para Diciembre
INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-12-01 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-12-01 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-12-02 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-12-02 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-12-03 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-12-03 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-12-04 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-12-04 20:35:18.906461+00', 40.7128, -74.0060, 10);

INSERT INTO income (date, latitude, longitude, altitude) VALUES ('2024-12-05 08:35:18.906461+00', 40.7128, -74.0060, 10);
INSERT INTO outcome (date, latitude, longitude, altitude) VALUES ('2024-12-05 20:35:18.906461+00', 40.7128, -74.0060, 10);

-- Insertar registros en schedules para Enero
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 1, 'worker', 1, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 2, 'worker', 2, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 3, 'worker', 3, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 4, 'worker', 4, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 5, 'worker', 5, 'worker');
-- Insertar registros en schedules para Febrero
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 6, 'worker', 6, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 7, 'worker', 7, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 8, 'worker', 8, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 9, 'worker', 9, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 10, 'worker', 10, 'worker');
-- Insertar registros en schedules para Marzo
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 11, 'worker', 11, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 12, 'worker', 12, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 13, 'worker', 13, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 14, 'worker', 14, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 15, 'worker', 15, 'worker');

-- Insertar registros en schedules para Abril
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 16, 'worker', 16, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 17, 'worker', 17, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 18, 'worker', 18, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 19, 'worker', 19, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 20, 'worker', 20, 'worker');
-- Insertar registros en schedules para Mayo
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 21, 'worker', 21, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 22, 'worker', 22, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 23, 'worker', 23, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 24, 'worker', 24, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 25, 'worker', 25, 'worker');

-- Insertar registros en schedules para Junio
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 26, 'worker', 26, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 27, 'worker', 27, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 28, 'worker', 28, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 29, 'worker', 29, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 30, 'worker', 30, 'worker');
-- Insertar registros en schedules para Julio
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 31, 'worker', 31, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 32, 'worker', 32, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 33, 'worker', 33, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 34, 'worker', 34, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 35, 'worker', 35, 'worker');
-- Insertar registros en schedules para Agosto
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 36, 'worker', 36, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 37, 'worker', 37, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 38, 'worker', 38, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 39, 'worker', 39, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 40, 'worker', 40, 'worker');

-- Insertar registros en schedules para Septiembre
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 41, 'worker', 41, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 42, 'worker', 42, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 43, 'worker', 43, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 44, 'worker', 44, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 45, 'worker', 45, 'worker');

-- Insertar registros en schedules para Octubre
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 46, 'worker', 46, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 47, 'worker', 47, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 48, 'worker', 48, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 49, 'worker', 49, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 50, 'worker', 50, 'worker');

-- Insertar registros en schedules para Noviembre
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 51, 'worker', 51, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 52, 'worker', 52, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 53, 'worker', 53, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 54, 'worker', 54, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 55, 'worker', 55, 'worker');

-- Insertar registros en schedules para Diciembre
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 56, 'worker', 56, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 57, 'worker', 57, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 58, 'worker', 58, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 59, 'worker', 59, 'worker');
INSERT INTO schedules (user_id, income_id, income_made_by, outcome_id, outcome_made_by) VALUES (6, 60, 'worker', 60, 'worker');
