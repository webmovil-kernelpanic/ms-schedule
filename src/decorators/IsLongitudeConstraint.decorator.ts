import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ async: false })
export class IsLongitudeConstraint implements ValidatorConstraintInterface {
  validate(longitude: any, args: ValidationArguments) {
    if (typeof longitude !== 'number') {
      return false; // No es un número, por lo tanto, la validación falla
    }
    return longitude >= -180 && longitude <= 180;
  }

  defaultMessage(args: ValidationArguments) {
    return 'Longitude must be a number between -180 and 180 degrees';
  }
}

export function IsLongitude(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsLongitudeConstraint,
    });
  };
}
