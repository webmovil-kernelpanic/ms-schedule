import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

// Decorador para validar latitud
@ValidatorConstraint({ async: false })
export class IsLatitudeConstraint implements ValidatorConstraintInterface {
  validate(latitude: any, args: ValidationArguments) {
    return typeof latitude === 'number' && latitude >= -90 && latitude <= 90;
  }

  defaultMessage(args: ValidationArguments) {
    return 'Latitude must be a number between -90 and 90 degrees';
  }
}

export function IsLatitude(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsLatitudeConstraint,
    });
  };
}
