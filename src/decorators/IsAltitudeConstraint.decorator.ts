import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ async: false })
export class IsAltitudeConstraint implements ValidatorConstraintInterface {
  validate(altitude: any, args: ValidationArguments) {
    return Number.isInteger(altitude);
  }

  defaultMessage(args: ValidationArguments) {
    return 'altitude must be an integer';
  }
}

export function IsAltitude(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsAltitudeConstraint,
    });
  };
}
