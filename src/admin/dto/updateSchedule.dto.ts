import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class UpdateScheduleDto {

  @ApiProperty({ type: Number, description: "User to change schedule", example: 5 })
  @IsNumber()
  user_id!: number;
  
  @ApiProperty({ type: String, description: "New user's date", example: "2024-06-18 18:40:05" })
  @IsString()
  new_time!: string;
}
