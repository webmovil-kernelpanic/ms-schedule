import { Controller, HttpException, HttpStatus, Request, Put, Body, UseGuards, Post, Get, Query } from '@nestjs/common';
import { AdminService } from './admin.service';
import { UpdateScheduleDto } from './dto/updateSchedule.dto';
import { ValidateAdminGuard } from 'src/guards/validateAdmin.guard';
import { AddScheduleDto } from './dto/addSchedule.dto';
import { DatePipe } from 'src/pipes/Date.pipe';

@Controller('admin')
export class AdminController {

    constructor(private readonly adminService: AdminService) { }

    /**
     * ----------------------------------------------
     * --------------------- PUT --------------------
     * ----------------------------------------------
     */

    @UseGuards(ValidateAdminGuard)
    @Put('update-income')
    updateIncome(@Request() req: Request, @Body() body: UpdateScheduleDto) {
        try {
            return this.adminService.updateIncome(body);
        } catch (e: unknown) {
            throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @UseGuards(ValidateAdminGuard)
    @Put('update-outcome')
    updateOutcome(@Request() req: Request, @Body() body: UpdateScheduleDto) {
        try {
            return this.adminService.updateOutcome(body);
        } catch (e: unknown) {
            throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @UseGuards(ValidateAdminGuard)
    @Put('update-weekly-table')
    updateWeeklyTable() {
        try {
            return this.adminService.updateWeeklyTable();
        } catch (e: unknown) {
            throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @UseGuards(ValidateAdminGuard)
    @Put('update-annual-table')
    updateAnnualTable() {
        try {
            return this.adminService.updateAnnualTable();
        } catch (e: unknown) {
            throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * ----------------------------------------------
     * --------------------- POST -------------------
     * ----------------------------------------------
     */

    @UseGuards(ValidateAdminGuard)
    @Post('add-income')
    addIncome(@Request() req: Request, @Body() body: AddScheduleDto) {
        try {
            return this.adminService.addIncome(body);
        } catch (e: unknown) {
            throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @UseGuards(ValidateAdminGuard)
    @Post('add-outcome')
    addOutcome(@Request() req: Request, @Body() body: AddScheduleDto) {
        try {
            return this.adminService.addOutcome(body);
        } catch (e: unknown) {
            throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * ----------------------------------------------
     * --------------------- GET --------------------
     * ----------------------------------------------
     */

    @UseGuards(ValidateAdminGuard)
    @Get('user-weekly-hours')
    async getUserWeeklyHours(
        @Query('userId') userId: number,
        @Query('weekStart') weekStart: string,
        @Request() req: Request,
    ) {
        try {
            return this.adminService.getUserWeeklyHours(userId, weekStart);
        } catch (e: unknown) {
            throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @UseGuards(ValidateAdminGuard)
    @Get('user-annual-hours')
    async getUserAnnualHours(
        @Query('userId') userId: number,
        @Query('currentYear') currentYear: number,
        @Request() req: Request,
    ) {
        try {
            return this.adminService.getUserAnnualHours(userId, currentYear);
        } catch (e: unknown) {
            throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
