import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('weekly_hours')
export class WeeklyHours {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ nullable: false })
  user_id!: number;
  
  @Column({ nullable: false, type: 'timestamp with time zone' })
  week_starts!: Date;

  @Column({ nullable: true, type: 'float' })
  mon_hours!: number;

  @Column({ nullable: true, type: 'float' })
  tue_hours!: number;

  @Column({ nullable: true, type: 'float' })
  wed_hours!: number;

  @Column({ nullable: true, type: 'float' })
  thu_hours!: number;

  @Column({ nullable: true, type: 'float' })
  fri_hours!: number;

  @Column({ nullable: true, type: 'float' })
  sat_hours!: number;

  @Column({ nullable: true, type: 'float' })
  sun_hours!: number;
}
