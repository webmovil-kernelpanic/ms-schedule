import {
    Column,
    Entity,
    JoinColumn,
    OneToOne,
    PrimaryGeneratedColumn,
  } from 'typeorm';
  
  @Entity('annual_hours')
  export class AnnualHours {
    @PrimaryGeneratedColumn()
    id!: number;
  
    @Column({ nullable: false })
    user_id!: number;

    @Column({ nullable: true, type: 'float' })
    year!: number;
  
    @Column({ nullable: true, type: 'float' })
    jan_hours!: number;
  
    @Column({ nullable: true, type: 'float' })
    feb_hours!: number;

    @Column({ nullable: true, type: 'float' })
    mar_hours!: number;

    @Column({ nullable: true, type: 'float' })
    apr_hours!: number;

    @Column({ nullable: true, type: 'float' })
    may_hours!: number;

    @Column({ nullable: true, type: 'float' })
    jun_hours!: number;

    @Column({ nullable: true, type: 'float' })
    jul_hours!: number;
    
    @Column({ nullable: true, type: 'float' })
    aug_hours!: number;

    @Column({ nullable: true, type: 'float' })
    sep_hours!: number;

    @Column({ nullable: true, type: 'float' })
    oct_hours!: number;

    @Column({ nullable: true, type: 'float' })
    nov_hours!: number;

    @Column({ nullable: true, type: 'float' })
    dec_hours!: number;
  }
  