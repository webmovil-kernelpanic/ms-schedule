import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Schedule } from 'src/entities/schedule.entity';
import { Income } from 'src/entities/income.entity';
import { Outcome } from 'src/entities/outcome.entity';
import { HttpModule } from '@nestjs/axios';
import { WeeklyHours } from './entities/weeklyHours.entity';
import { AnnualHours } from './entities/annualHours.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Schedule, Income, Outcome, WeeklyHours, AnnualHours]),
    HttpModule.register({ timeout: 5000, maxRedirects: 5 }),
    HttpModule,
  ],  
  controllers: [AdminController],
  providers: [AdminService],
})
export class AdminModule {}
