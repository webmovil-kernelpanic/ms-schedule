import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, Repository } from 'typeorm';
import { UpdateScheduleDto } from './dto/updateSchedule.dto';
import { Schedule } from 'src/entities/schedule.entity';
import { Income } from 'src/entities/income.entity';
import { Outcome } from 'src/entities/outcome.entity';
import { format } from 'date-fns';
import { AddScheduleDto } from './dto/addSchedule.dto';
import { WeeklyHours } from './entities/weeklyHours.entity';
import { AnnualHours } from './entities/annualHours.entity';
import { setHours, subHours } from 'date-fns';

/**
 * Interface representing the selected user's day information.
 */
interface SelectedUserDay {
  schedule_id: number;
  user_id?: number;
  income_made_by?: string;
  outcome_made_by?: string;
  income_id?: number;
  outcome_id?: number;
  date?: Date;
  latitude?: number;
  longitude?: number;
  altitude?: number;
}

@Injectable()
export class AdminService {
  constructor(
    @InjectRepository(Schedule)
    private readonly scheduleRepository: Repository<Schedule>,

    @InjectRepository(Income)
    private readonly incomeRepository: Repository<Income>,

    @InjectRepository(Outcome)
    private readonly outcomeRepository: Repository<Outcome>,

    @InjectRepository(WeeklyHours)
    private readonly weeklyHoursRepository: Repository<WeeklyHours>,

    @InjectRepository(AnnualHours)
    private readonly annualHoursRepository: Repository<AnnualHours>,
  ) {}

  /**
   * Updates the income record for a given user and day.
   *
   * @param updateSchedule - The data transfer object containing update information.
   * @returns An object with status and updated user information.
   * @throws Error if no record is found for the given user_id and date.
   */
  async updateIncome(
    updateSchedule: UpdateScheduleDto,
  ): Promise<{ status: string; updatedUser: number; newIncome: string }> {
    // Formatear newTime a 'yyyy-MM-dd' para la comparación
    const formattedDate = format(
      new Date(updateSchedule.new_time),
      'yyyy-MM-dd',
    );

    const selectedUserDay: SelectedUserDay | undefined =
      await this.scheduleRepository
        .createQueryBuilder('s')
        .innerJoinAndSelect('income', 'i', 's.income_id = i.income_id')
        .select(['s.schedule_id as schedule_id', 's.income_id as income_id'])
        .where("TO_CHAR(i.date, 'yyyy-MM-dd') = :formattedDate", {
          formattedDate: formattedDate,
        })
        .andWhere('s.user_id = :userId', { userId: updateSchedule.user_id })
        .getRawOne();

    if (!selectedUserDay) {
      throw new Error(
        `No se encontró ningún registro para user_id ${updateSchedule.user_id} en la fecha ${updateSchedule.new_time}`,
      );
    }

    // UPDATES SCHEDULE TABLE
    await this.scheduleRepository.update(
      {
        schedule_id: selectedUserDay.schedule_id,
        user_id: updateSchedule.user_id,
      },
      { income_made_by: 'admin' },
    );

    // UPDATES INCOME TABLE usando consulta SQL cruda
    await this.incomeRepository
      .createQueryBuilder()
      .update(Income)
      .set({ date: new Date(updateSchedule.new_time) })
      .where(
        "income_id = :incomeId AND TO_CHAR(date, 'yyyy-MM-dd') = :formattedDate",
        {
          incomeId: selectedUserDay.income_id,
          formattedDate: formattedDate,
        },
      )
      .execute();

    return {
      status: 'SUCCESS',
      updatedUser: updateSchedule.user_id,
      newIncome: updateSchedule.new_time,
    };
  }

  /**
   * Updates the outcome record for a given user and day.
   *
   * @param updateSchedule - The data transfer object containing update information.
   * @returns An object with status and updated user information.
   * @throws Error if no record is found for the given user_id and date.
   */
  async updateOutcome(
    updateSchedule: UpdateScheduleDto,
  ): Promise<{ status: string; updatedUser: number; newOutcome: string }> {
    // Formatear newTime a 'yyyy-MM-dd' para la comparación
    const formattedDate = format(
      new Date(updateSchedule.new_time),
      'yyyy-MM-dd',
    );

    const selectedUserDay: SelectedUserDay | undefined =
      await this.scheduleRepository
        .createQueryBuilder('s')
        .innerJoinAndSelect('outcome', 'i', 's.outcome_id = i.outcome_id')
        .select(['s.schedule_id as schedule_id', 's.outcome_id as outcome_id'])
        .where("TO_CHAR(i.date, 'yyyy-MM-dd') = :formattedDate", {
          formattedDate: formattedDate,
        })
        .andWhere('s.user_id = :userId', { userId: updateSchedule.user_id })
        .getRawOne();

    if (!selectedUserDay) {
      throw new Error(
        `No se encontró ningún registro para user_id ${updateSchedule.user_id} en la fecha ${updateSchedule.new_time}`,
      );
    }

    // UPDATES SCHEDULE TABLE
    await this.scheduleRepository.update(
      {
        schedule_id: selectedUserDay.schedule_id,
        user_id: updateSchedule.user_id,
      },
      { outcome_made_by: 'admin' },
    );

    // UPDATES OUTCOME TABLE usando consulta SQL cruda
    await this.outcomeRepository
      .createQueryBuilder()
      .update(Outcome)
      .set({ date: new Date(updateSchedule.new_time) })
      .where(
        "outcome_id = :outcomeId AND TO_CHAR(date, 'yyyy-MM-dd') = :formattedDate",
        {
          outcomeId: selectedUserDay.outcome_id,
          formattedDate: formattedDate,
        },
      )
      .execute();

    return {
      status: 'SUCCESS',
      updatedUser: updateSchedule.user_id,
      newOutcome: updateSchedule.new_time,
    };
  }

  /**
   * Admin manually adds income record
   *
   * @param addSchedule - The data transfer object containing update information.
   * @returns An object with status and updated user information.
   * @throws Error if no record is found for the given user_id and date.
   */
  async addIncome(
    addSchedule: AddScheduleDto,
  ): Promise<{ status: string; updatedUser: number; newOutcome: string }> {
    // Inserts into INCOME TABLE
    const newIncome = await this.incomeRepository.save({
      date: new Date(addSchedule.new_time),
      altitude: 0,
      longitude: 0,
      latitude: 0,
    });

    // Inserts into SCHEDULE TABLE
    await this.scheduleRepository.save({
      user_id: addSchedule.user_id,
      income: newIncome,
      income_made_by: 'admin',
    });

    return {
      status: 'SUCCESS',
      updatedUser: addSchedule.user_id,
      newOutcome: addSchedule.new_time,
    };
  }

  /**
   * Admin manually adds outcome record
   *
   * @param addSchedule - The data transfer object containing update information.
   * @returns An object with status and updated user information.
   * @throws Error if no record is found for the given user_id and date.
   */
  async addOutcome(
    addSchedule: AddScheduleDto,
  ): Promise<{ status: string; updatedUser: number; newOutcome: string }> {
    // Formatear newTime a 'yyyy-MM-dd' para la comparación
    const formattedDate = format(new Date(addSchedule.new_time), 'yyyy-MM-dd');

    const selectedUserDay: SelectedUserDay | undefined =
      await this.scheduleRepository
        .createQueryBuilder('s')
        .innerJoinAndSelect('income', 'i', 's.income_id = i.income_id')
        .select('*')
        .where(
          "TO_CHAR(i.date, 'yyyy-MM-dd') = :formattedDate AND s.user_id = :user_id",
          {
            formattedDate: formattedDate,
            user_id: addSchedule.user_id,
          },
        )
        .getRawOne();

    if (!selectedUserDay) {
      throw new Error(
        `No se encontró ningún registro para user_id ${addSchedule.user_id} en la fecha ${addSchedule.new_time}`,
      );
    }

    if (selectedUserDay.outcome_id === null) {
      // Inserts into OUTCOME TABLE
      const newOutcome = await this.outcomeRepository.save({
        date: new Date(addSchedule.new_time),
        altitude: 0,
        longitude: 0,
        latitude: 0,
      });
      // UPDATES SCHEDULE TABLE
      await this.scheduleRepository.update(
        {
          schedule_id: selectedUserDay.schedule_id,
          user_id: addSchedule.user_id,
        },
        { outcome_made_by: 'admin', outcome: newOutcome },
      );
      return {
        status: 'SUCCESS',
        updatedUser: addSchedule.user_id,
        newOutcome: addSchedule.new_time,
      };
    }

    throw new Error('El usuario ya tiene outcome usa el endpoint para PUT');
  }

  async getUserWeeklyHours(
    userId: number,
    weekStart: string,
  ): Promise<{ status: string; userWeeklyHours: number[] }> {
    try {      

      // Obtener las horas semanales para el usuario y la fecha de inicio de la semana
      const weeklyHours = await this.weeklyHoursRepository
        .createQueryBuilder()
        .select('*')
        .where(
          "TO_CHAR(week_starts, 'yyyy-MM-dd') = :formattedDate AND user_id = :user_id",
          {
            formattedDate: weekStart,
            user_id: userId,
          },
        )
        .getRawOne();

      if (!weeklyHours) {
        throw new Error(
          `Weekly hours not found for user ${userId} starting from ${format(weekStart, 'yyyy-MM-dd')}`,
        );
      }

      // Extraer horas de cada día en el orden correcto (lunes a domingo)
      const userWeeklyHours = [
        weeklyHours.mon_hours,
        weeklyHours.tue_hours,
        weeklyHours.wed_hours,
        weeklyHours.thu_hours,
        weeklyHours.fri_hours,
        weeklyHours.sat_hours,
        weeklyHours.sun_hours,
      ];

      return { status: 'SUCCESS', userWeeklyHours };
    } catch (error) {
      throw new Error('Failed to retrieve user weekly hours');
    }
  }

  async getUserAnnualHours(
    userId: number,
    currentYear: number,
  ): Promise<{ status: string; userAnnualHours: number[] }> {

    try{
      // Obtener las horas semanales para el usuario y la fecha de inicio de la semana
      const annualHours = await this.annualHoursRepository
        .createQueryBuilder()
        .select('*')
        .where(
          "year = :currentYear AND user_id = :user_id",
          {
            currentYear: currentYear,
            user_id: userId,
          },
        )
        .getRawOne();

      if (!annualHours) {
        throw new Error(
          `Annual hours not found for user ${userId} from year ${currentYear}`,
        );
      }

      // Extraer horas de cada día en el orden correcto (lunes a domingo)
      const userAnnualHours = [
        annualHours.jan_hours,
        annualHours.feb_hours,
        annualHours.mar_hours,
        annualHours.apr_hours,
        annualHours.may_hours,
        annualHours.jun_hours,
        annualHours.jul_hours,
        annualHours.aug_hours,
        annualHours.sep_hours,
        annualHours.oct_hours,
        annualHours.nov_hours,
        annualHours.dec_hours,
      ];

      return { status: 'SUCCESS', userAnnualHours };

    }catch(error){
      throw new Error('Failed to retrieve user annual hours');
    }
  }

  async updateWeeklyTable() {

    await this.weeklyHoursRepository.clear();      
    
    const users = await this.scheduleRepository
      .createQueryBuilder('schedule')
      .select('schedule.user_id as user_id')
      .distinct(true)
      .getRawMany();

    console.log('Users:', users);

    const startOfYear = new Date(new Date().getFullYear(), 0, 1);
    const currentDate = new Date(); // Fecha actual

    for (const user of users) {
      const userId = user.user_id;

      let currentWeekStart = new Date(new Date().getFullYear(), 0, 1);

      while (
        currentWeekStart <= currentDate &&
        currentWeekStart.getFullYear() === startOfYear.getFullYear()
      ) {
        const currentWeekEnd = new Date(currentWeekStart);
        currentWeekEnd.setDate(currentWeekStart.getDate() + 6);

        if (currentWeekEnd > currentDate) {
          // Ajustar el final de la semana para no pasar de la fecha actual
          currentWeekEnd.setDate(currentDate.getDate());
        }

        const schedules = await this.scheduleRepository.find({
          where: {
            user_id: userId,
            income: {
              date: Between(currentWeekStart, currentWeekEnd),
            },
          },
          relations: ['income', 'outcome'],
        });

        console.log(
          `Schedules for user ${userId} from ${currentWeekStart} to ${currentWeekEnd}:`,
          schedules,
        );

        const weeklyHours = {
          user_id: userId,
          week_starts: currentWeekStart,
          mon_hours: 0,
          tue_hours: 0,
          wed_hours: 0,
          thu_hours: 0,
          fri_hours: 0,
          sat_hours: 0,
          sun_hours: 0,
        };

        for (const schedule of schedules) {
          const day = schedule.income.date.getDay(); // 0 = Sunday, 1 = Monday, ..., 6 = Saturday
          let hoursWorked = 0;
          if (schedule.outcome == null) {
            hoursWorked = 0;
          } else {
            hoursWorked =
              (schedule.outcome.date.getTime() -
                schedule.income.date.getTime()) /
              (1000 * 60 * 60);
          }

          switch (day) {
            case 1:
              weeklyHours.mon_hours += hoursWorked;
              break;
            case 2:
              weeklyHours.tue_hours += hoursWorked;
              break;
            case 3:
              weeklyHours.wed_hours += hoursWorked;
              break;
            case 4:
              weeklyHours.thu_hours += hoursWorked;
              break;
            case 5:
              weeklyHours.fri_hours += hoursWorked;
              break;
            case 6:
              weeklyHours.sat_hours += hoursWorked;
              break;
            case 0:
              weeklyHours.sun_hours += hoursWorked;
              break;
          }
        }

        // Asegurarse de que todos los valores sean números antes de redondear
        weeklyHours.mon_hours =
          parseFloat(weeklyHours.mon_hours.toFixed(2)) || 0;
        weeklyHours.tue_hours =
          parseFloat(weeklyHours.tue_hours.toFixed(2)) || 0;
        weeklyHours.wed_hours =
          parseFloat(weeklyHours.wed_hours.toFixed(2)) || 0;
        weeklyHours.thu_hours =
          parseFloat(weeklyHours.thu_hours.toFixed(2)) || 0;
        weeklyHours.fri_hours =
          parseFloat(weeklyHours.fri_hours.toFixed(2)) || 0;
        weeklyHours.sat_hours =
          parseFloat(weeklyHours.sat_hours.toFixed(2)) || 0;
        weeklyHours.sun_hours =
          parseFloat(weeklyHours.sun_hours.toFixed(2)) || 0;

        console.log(`Weekly hours data for user ${userId}:`, weeklyHours);

        // Insertar en la base de datos
        await this.weeklyHoursRepository.save(weeklyHours);

        // Avanzar a la próxima semana
        currentWeekStart.setDate(currentWeekStart.getDate() + 7);
      }
    }
  }
  async updateAnnualTable() {

    await this.annualHoursRepository.clear();

    const users = await this.scheduleRepository
      .createQueryBuilder('schedule')
      .select('DISTINCT schedule.user_id as user_id')
      .getRawMany();

    const currentYear = new Date().getFullYear();

    for (const user of users) {
      const userId = user.user_id;

      const annualHours = new AnnualHours();
      annualHours.user_id = userId;
      annualHours.year = currentYear;

      // Initialize monthly hours to zero
      annualHours.jan_hours = 0;
      annualHours.feb_hours = 0;
      annualHours.mar_hours = 0;
      annualHours.apr_hours = 0;
      annualHours.may_hours = 0;
      annualHours.jun_hours = 0;
      annualHours.jul_hours = 0;
      annualHours.aug_hours = 0;
      annualHours.sep_hours = 0;
      annualHours.oct_hours = 0;
      annualHours.nov_hours = 0;
      annualHours.dec_hours = 0;

      const startOfYear = new Date(`${currentYear}-01-01T00:00:00Z`);
      const endOfYear = new Date(`${currentYear + 1}-01-01T00:00:00Z`);

      const schedules = await this.scheduleRepository.find({
        where: {
          user_id: userId,
          income: {
            date: Between(startOfYear, endOfYear),
          },
        },
        relations: ['income', 'outcome'],
      });

      // Calculate monthly hours
      for (const schedule of schedules) {
        const month = schedule.income.date.getUTCMonth(); // Using getUTCMonth to avoid timezone issues

        let hoursWorked = 0;
        if (schedule.outcome) {
          hoursWorked =
            (schedule.outcome.date.getTime() - schedule.income.date.getTime()) /
            (1000 * 60 * 60);
        }

        switch (month) {
          case 0:
            annualHours.jan_hours += hoursWorked;
            break;
          case 1:
            annualHours.feb_hours += hoursWorked;
            break;
          case 2:
            annualHours.mar_hours += hoursWorked;
            break;
          case 3:
            annualHours.apr_hours += hoursWorked;
            break;
          case 4:
            annualHours.may_hours += hoursWorked;
            break;
          case 5:
            annualHours.jun_hours += hoursWorked;
            break;
          case 6:
            annualHours.jul_hours += hoursWorked;
            break;
          case 7:
            annualHours.aug_hours += hoursWorked;
            break;
          case 8:
            annualHours.sep_hours += hoursWorked;
            break;
          case 9:
            annualHours.oct_hours += hoursWorked;
            break;
          case 10:
            annualHours.nov_hours += hoursWorked;
            break;
          case 11:
            annualHours.dec_hours += hoursWorked;
            break;
        }
      }

      // Divide each value by 20 and round to two decimal places
      annualHours.jan_hours = parseFloat((annualHours.jan_hours / 20).toFixed(2)) || 0;
      annualHours.feb_hours = parseFloat((annualHours.feb_hours / 20).toFixed(2)) || 0;
      annualHours.mar_hours = parseFloat((annualHours.mar_hours / 20).toFixed(2)) || 0;
      annualHours.apr_hours = parseFloat((annualHours.apr_hours / 20).toFixed(2)) || 0;
      annualHours.may_hours = parseFloat((annualHours.may_hours / 20).toFixed(2)) || 0;
      annualHours.jun_hours = parseFloat((annualHours.jun_hours / 20).toFixed(2)) || 0;
      annualHours.jul_hours = parseFloat((annualHours.jul_hours / 20).toFixed(2)) || 0;
      annualHours.aug_hours = parseFloat((annualHours.aug_hours / 20).toFixed(2)) || 0;
      annualHours.sep_hours = parseFloat((annualHours.sep_hours / 20).toFixed(2)) || 0;
      annualHours.oct_hours = parseFloat((annualHours.oct_hours / 20).toFixed(2)) || 0;
      annualHours.nov_hours = parseFloat((annualHours.nov_hours / 20).toFixed(2)) || 0;
      annualHours.dec_hours = parseFloat((annualHours.dec_hours / 20).toFixed(2)) || 0;

      // Save or update the annual hours record
      await this.annualHoursRepository.save(annualHours);
    }
  }
}
