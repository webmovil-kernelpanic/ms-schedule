import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Income } from './income.entity';
import { Outcome } from './outcome.entity';

@Entity('schedules')
export class Schedule {
  @PrimaryGeneratedColumn()
  schedule_id!: number;

  @Column({ nullable: false })
  user_id!: number;

  @OneToOne(() => Income)
  @JoinColumn({ name: 'income_id' })
  income!: Income;

  @Column({ nullable: false })
  income_made_by!: string;

  @OneToOne(() => Outcome, { nullable: true })
  @JoinColumn({ name: 'outcome_id' })
  outcome?: Outcome;

  @Column({ nullable: true })
  outcome_made_by?: string;
}
