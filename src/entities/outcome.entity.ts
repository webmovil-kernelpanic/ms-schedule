import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('outcome')
export class Outcome {
  @PrimaryGeneratedColumn()
  outcome_id!: number;
  @Column({ nullable: false, type: 'timestamp with time zone' })
  date!: Date;
  @Column({ type: 'float8' })
  latitude!: number;
  @Column({ type: 'float8' })
  longitude!: number;
  @Column({ type: 'float8' })
  altitude!: number;
}
