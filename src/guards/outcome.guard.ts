import {
  CanActivate,
  ExecutionContext,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { AppService } from 'src/app.service';
import { UserDto } from 'src/dto/User.dto';
import { Schedule } from 'src/entities/schedule.entity';

@Injectable()
export class OutcomeGuard implements CanActivate {
  constructor(private scheduleService: AppService) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const user: UserDto = context.switchToHttp().getRequest().user;
    const records = await this.scheduleService.findRecordsForToday();
    const userRecord: Schedule | undefined = records.find(
      (schedule) => schedule.user_id == user.id,
    );
    if (!userRecord) {
      const response = context.switchToHttp().getResponse();
      response.status(HttpStatus.FORBIDDEN).json({
        message: 'User has to set the income first',
      });
      return false;
    }
    if (userRecord.outcome) {
      const response = context.switchToHttp().getResponse();
      response.status(HttpStatus.FORBIDDEN).json({
        message: 'User already set his income and outcome',
      });
      return false;
    }
    //si no hay income no debería ingresar outcome --check
    //si ya registró salida por no debería ingreasr otro outcome --check
    return true;
  }
}
