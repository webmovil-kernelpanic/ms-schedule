import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { Observable, isEmpty } from 'rxjs';
import { AppService } from 'src/app.service';
import { UserDto } from 'src/dto/User.dto';
import { Schedule } from 'src/entities/schedule.entity';

@Injectable()
export class IncomeGuard implements CanActivate {
  constructor(private scheduleService: AppService) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const user: UserDto = context.switchToHttp().getRequest().user;
    const records = await this.scheduleService.findRecordsForToday();

    const userRecord: Schedule | undefined = records.find(
      (schedule) => schedule.user_id == user.id,
    );
    if (userRecord) {
      const response = context.switchToHttp().getResponse();
      response.status(HttpStatus.FORBIDDEN).json({
        message: 'User has already set his Income for today',
      });

      return false;
    }
    return true;
  }
}
