import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Request } from 'express';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom } from 'rxjs';

interface User {
  id: number;
  name: string;
  email: string;
  birthday: string;
  role: string;
}


@Injectable()
export class ValidateAdminGuard implements CanActivate {
  constructor(private readonly httpService: HttpService) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = this.extractTokenFromHeader(request);
    if (!token) {
      throw new UnauthorizedException();
    }
    try {
      const response = await firstValueFrom(
        this.httpService.get(`${process.env.MS_USERS_URL}/users`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        }),
      );
      if (response.status == 200) {
        const user: User = response.data;

        // Si el usuario no es admin arroja un error 
        if (user.role != 'admin') {
          throw new UnauthorizedException();
        }

        // En caso de ser admin no arroja nada

      } else {
        throw new UnauthorizedException();
      }
    } catch {
      throw new UnauthorizedException();
    }
    return true;
  }

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }
}
