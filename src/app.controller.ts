import {
  Request,
  Controller,
  Get,
  Post,
  UseGuards,
  Param,
  Query,
  ParseIntPipe,
  Body,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { AppService } from './app.service';
import { UserDto } from './dto/User.dto';
import { ValidateTokenGuard } from './guards/validateToken.guard';
import { IncomeGuard } from './guards/income.guard';
import { OutcomeGuard } from './guards/outcome.guard';
import { DatePipe } from './pipes/Date.pipe';
import { ValidateAdminGuard } from './guards/validateAdmin.guard';
import { IncomeDto } from './dto/income.dto';
import { OutcomeDto } from './dto/outcome.dto';

declare module '@nestjs/common'{
  interface Request{
    user?: UserDto; // extends the parameter in the interface
  }
}

@Controller('schedule')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @UseGuards(ValidateTokenGuard)
  @Get('check-status')
  async getStatus(@Request() req: Request) {
    try{
      if(req.user){
        return this.appService.getUserStatus(req.user);
      }
      throw new Error("No existe el usuario")
    }catch(e: unknown){
      throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UseGuards(ValidateTokenGuard, IncomeGuard)
  @Post('income')
  async setIncome(@Request() req: Request, @Body() body: IncomeDto) {
    try{
      if(req.user){
        const user: UserDto = req.user;
        return this.appService.createIncome(user, body);
      }
      throw new Error("No existe el usuario")
    }catch(e: unknown){
      throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @UseGuards(ValidateTokenGuard, OutcomeGuard)
  @Post('outcome')
  async setOutcome(@Request() req: Request, @Body() body: OutcomeDto) {    
    try{
      if(req.user){
        const user: UserDto = req.user;
        return this.appService.createOutcome(user, body);
      }
      throw new Error("No existe el usuario")
    }catch(e: unknown){
      throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Get()
  @UseGuards(ValidateTokenGuard)
  async getInterval(
    @Query('initDate', DatePipe) initDate: Date,
    @Query('finishDate', DatePipe) finishDate: Date,
    @Request() req: Request,
  ) {    
    try{
      if(req.user){
        const user: UserDto = req.user;
        return await this.appService.findRecordsByInterval(
          initDate,
          finishDate,
          user.id,
        );
      }
      throw new Error("No existe el usuario")
    }catch(e: unknown){
      throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }


  //Esta Ruta es utilizada por los admin para checkear la semana actual
  @UseGuards(ValidateAdminGuard)
  @Get(':userId')
  async getIntervalForAdmin(
    @Query('initDate', DatePipe) initDate: Date,
    @Query('finishDate', DatePipe) finishDate: Date,
    @Param('userId', ParseIntPipe) userId: number,
    @Request() req: Request,
  ) {
    return await this.appService.findRecordsByInterval(
      initDate,
      finishDate,
      userId,
    );
  }
}
