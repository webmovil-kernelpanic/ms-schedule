import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { UserDto } from './dto/User.dto';
import { HttpService } from '@nestjs/axios';
import { Schedule } from './entities/schedule.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, DataSource, Repository, getManager } from 'typeorm';
import { setHours, subHours } from 'date-fns';
import { IncomeDto } from './dto/income.dto';
import { Income } from './entities/income.entity';
import { Outcome } from './entities/outcome.entity';
import { OutcomeDto } from './dto/outcome.dto';

interface Record {
  schedule_id: number;

  user_id: number;

  income_id: number;
  income_date: Date;
  income_latitude: number;
  income_longitude: number;
  income_altitude: number;
  income_made_by: string;

  outcome_id: number;
  outcome_date: Date;
  outcome_latitude: number;
  outcome_longitude: number;
  outcome_altitude: number;
  outcome_made_by: string;
}

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Schedule)
    private readonly scheduleRepository: Repository<Schedule>,
    @InjectRepository(Income)
    private readonly incomeRepository: Repository<Income>,
    @InjectRepository(Outcome)
    private readonly outcomeRepository: Repository<Outcome>,
    private datasource: DataSource,
  ) {}
  async createIncome(user: UserDto, income: IncomeDto) {
    try {
      const savedIncome = await this.incomeRepository.save({
        date: new Date(),
        altitude: income.altitude,
        longitude: income.longitude,
        latitude: income.latitude,
      });

      if (savedIncome) {
        const newSchedule: Partial<Schedule> = {
          user_id: user.id,
          income: savedIncome,
          income_made_by: 'worker',
          outcome: undefined,
          outcome_made_by: undefined,
        };
        return this.scheduleRepository.save(newSchedule);
      }
      throw new HttpException('Error on saving Income', HttpStatus.NOT_FOUND);
    } catch (error: unknown) {
      throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  async createOutcome(user: UserDto, outcome: OutcomeDto): Promise<Schedule> {
    const now = new Date();
    const startOfDay = new Date(now);
    startOfDay.setHours(0, 0, 0, 0);
    const endOfDay = new Date(now);
    endOfDay.setHours(43, 59, 59, 999);
    try {
      const userRecord = await this.scheduleRepository.findOne({
        where: {
          user_id: user.id,
          outcome: undefined,
          income: {
            date: Between(startOfDay, endOfDay),
          },
        },
        relations: ['income', 'outcome'],
      });
      if (userRecord) {
        const savedOutcome = await this.outcomeRepository.save({
          date: now,
          altitude: outcome.altitude,
          latitude: outcome.latitude,
          longitude: outcome.longitude,
        });
        if (savedOutcome) {
          userRecord.outcome = savedOutcome;
          userRecord.outcome_made_by = 'worker';
          return await this.scheduleRepository.save(userRecord);
        } else {
          throw new HttpException(
            'Error on saving outcome',
            HttpStatus.INTERNAL_SERVER_ERROR,
          );
        }
      }
      throw new HttpException(
        'Error on finding the user´s records',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    } catch (error) {
      throw new HttpException('Forbidden', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
  async findRecordsByInterval(
    initDate: Date,
    finishDate: Date,
    userId: number,
  ): Promise<Schedule[]> {
    finishDate.setHours(43, 59, 59);
    return this.scheduleRepository.find({
      where: {
        user_id: userId,
        income: {
          date: Between(initDate, finishDate),
        },
      },
      relations: ['income', 'outcome'],
    });
  }
  async findRecordsForToday(): Promise<Schedule[]> {
    const today = subHours(new Date(), 4);
    const todayStr = today.toISOString().split('T')[0]; // Obtener la parte de yyyy-mm-dd

    const queryRunner = this.datasource.createQueryRunner();

    try {
      await queryRunner.connect();
      const records = await queryRunner.query(
        `
        SELECT s.*, i.*, o.*
        FROM schedules s
        LEFT JOIN income i ON s.income_id = i.income_id
        LEFT JOIN outcome o ON s.outcome_id = o.outcome_id
        WHERE DATE_TRUNC('day', i.date) = $1
        `,
        [todayStr],
      );

      // Convertir los resultados en instancias de las entidades
      return records.map((record: Record) => {
        const schedule = new Schedule();
        Object.assign(schedule, {
          schedule_id: record.schedule_id,
          user_id: record.user_id,
          income_made_by: record.income_made_by,
          outcome_made_by: record.outcome_made_by,
        });

        if (record.income_id) {
          schedule.income = {
            income_id: record.income_id,
            date: record.income_date,
            latitude: record.income_latitude,
            longitude: record.income_longitude,
            altitude: record.income_altitude,
          } as Income;
        }

        if (record.outcome_id) {
          schedule.outcome = {
            outcome_id: record.outcome_id,
            date: record.outcome_date,
            latitude: record.outcome_latitude,
            longitude: record.outcome_longitude,
            altitude: record.outcome_altitude,
          } as Outcome;
        }

        return schedule;
      });
    } finally {
      await queryRunner.release();
    }
  }
  //Todo -> este código se puede mejorar bastante
  async getUserStatus(
    user: UserDto,
  ): Promise<{ status_description: string; status_code: number }> {
    try {
      // Comprobar que haya registrado su entrada
      const checkUser = await this.scheduleRepository
        .createQueryBuilder('schedule')
        .leftJoinAndSelect('schedule.income', 'income')
        .leftJoinAndSelect('schedule.outcome', 'outcome')
        .where('schedule.user_id = :user_id', { user_id: user.id })
        .orderBy('income.date', 'DESC')
        .getOne();

      console.log(checkUser);

      // El usuario no tiene ningun registro. Primera vez utilizando la app
      if (!checkUser) {
        return {
          status_description:
            'El usuario NO tiene entrada ni salida registrada',
          status_code: 0,
        };
      }

      // Revisar si el usuario no tiene registros hoy.
      if (
        checkUser.income.date.toLocaleDateString() !==
        new Date().toLocaleDateString()
      ) {
        return {
          status_description:
            'El usuario NO tiene entrada ni salida registrada',
          status_code: 0,
        };
      }

      // No deberia existir el registro si no tiene income, pero por si acaso.
      if (checkUser.income === null) {
        return {
          status_description:
            'El usuario NO tiene entrada ni salida registrada',
          status_code: 0,
        };
      }

      // Revisar si tiene entrada y no salida.
      if (checkUser.income?.date !== null && checkUser.outcome === null) {
        return {
          status_description: 'El usuario tiene solo entrada',
          status_code: 1,
        };
      }

      // Revisar si tiene salida y entrada.
      else if (checkUser.income !== null && checkUser.outcome !== null) {
        console.log(checkUser);
        return {
          status_description: 'El usuario tiene entrada y salida registrada',
          status_code: 2,
        };
      }
      throw new Error('No se entró en ninguo de los casos del algoritmo');
    } catch (error) {
      if (error instanceof BadRequestException) {
        throw error; // Si ya ingresó su entrada hoy, lanzar la excepción BadRequestException
      } else {
        throw new UnauthorizedException('Token inválido');
      }
    }
  }
}

// async validateToken(user: UserDto): Promise<Date> {
//   try {
//     const incomedate: Date = new Date();
//     const schedule = {
//       user_id: user.id,
//       income: incomedate,
//       outcome: null,
//     };

//     // Comprobar que no se haya registrado durante el dia
//     const checkUser = await this.scheduleRepository
//       .createQueryBuilder('schedule')
//       .where('schedule.user_id = :user_id', { user_id: user.id })
//       .orderBy('schedule.income', 'DESC')
//       .getOne();

//     if (checkUser) {
//       const todaysDay = new Date().toISOString().split('T')[0]; // Obtener la fecha actual en formato YYYY-MM-DD

//       if (checkUser.income.toISOString().split('T')[0] === todaysDay) {
//         throw new BadRequestException('El usuario ya ingresó su entrada hoy');
//       } else {
//         // El usuario aún no ha ingresado su entrada hoy
//         this.scheduleRepository.save(schedule);
//       }
//     } else {
//       // Aqui se inserta la entrada en caso de que el usuario no tenga ningun registro en la bd
//       this.scheduleRepository.save(schedule);
//     }

//     return incomedate;
//   } catch (error) {
//     console.error(error);
//     if (error instanceof BadRequestException) {
//       throw error; // Si ya ingresó su entrada hoy, lanzar la excepción BadRequestException
//     } else {
//       throw new UnauthorizedException('Token inválido');
//     }
//   }
// // }

//  try {
//    const outcomedate: Date = new Date();

//    // Comprobar que haya registrado su entrada
//    const checkUser = await this.scheduleRepository
//      .createQueryBuilder('schedule')
//      .where('schedule.user_id = :user_id', { user_id: user.id })
//      .orderBy('schedule.income', 'DESC')
//      .getOne();

//    if (checkUser) {
//      if (checkUser.outcome != null) {
//        throw new BadRequestException('El usuario ya registro su salida hoy');
//      }

//      const todaysDay = new Date().toISOString().split('T')[0]; // Obtener la fecha actual en formato YYYY-MM-DD

//      if (checkUser.income.toISOString().split('T')[0] === todaysDay) {
//        // El usuario ya ingreso su entrada entonces se agrega la salida
//        await this.scheduleRepository.update(checkUser.schedule_id, {
//          outcome: outcomedate,
//        });
//      } else {
//        // El usuario aún no ha ingresado su entrada hoy
//        throw new BadRequestException(
//          'El usuario aun no ha ingresado su entrada hoy',
//        );
//      }
//    } else {
//      // Aqui se inserta la entrada en caso de que el usuario no tenga ningun registro en la bd
//      throw new BadRequestException(
//        'El usuario no tiene ningun registro en la bd',
//      );
//    }

//    return outcomedate;
//  } catch (error) {
//    console.error(error);
//    if (error instanceof BadRequestException) {
//      throw error; // Si ya ingresó su entrada hoy, lanzar la excepción BadRequestException
//    } else {
//      throw new UnauthorizedException('Token inválido');
//    }
//  }
