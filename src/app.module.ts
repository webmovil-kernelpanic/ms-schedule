import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Schedule } from './entities/schedule.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';
import { Income } from './entities/income.entity';
import { Outcome } from './entities/outcome.entity';
import { AdminModule } from './admin/admin.module';
import { WeeklyHours } from './admin/entities/weeklyHours.entity';
import { AnnualHours } from './admin/entities/annualHours.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT || '5432'),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      entities: [Schedule, Income, Outcome, WeeklyHours, AnnualHours],
      synchronize: true,
    }),
    HttpModule.register({ timeout: 5000, maxRedirects: 5 }),
    HttpModule,
    TypeOrmModule.forFeature([Schedule, Income, Outcome]),
    AdminModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
