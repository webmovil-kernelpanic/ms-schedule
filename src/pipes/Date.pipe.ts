import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';

@Injectable()
export class DatePipe implements PipeTransform<string, Date> {
  transform(value: string, metadata: ArgumentMetadata): Date {
    const dateValue = new Date(value);
    if (isNaN(dateValue.getTime())) {
      throw new BadRequestException(
        `Invalid date format. Date should be in ISO format. Current Value ${value}. Must to be YYYY-MM-DD`,
      );
    }
    return dateValue;
  }
}
